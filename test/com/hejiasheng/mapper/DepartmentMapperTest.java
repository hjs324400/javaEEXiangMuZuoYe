package com.hejiasheng.mapper;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import com.shuangyulin.po.Department;


public class DepartmentMapperTest {

	@BeforeEach
	void setUp() throws Exception {
		// 创建sqlSessionFactory

		// mybatis配置文件
		String resource = "SqlMapConfig.xml";
		// 得到配置文件流
		InputStream inputStream = Resources.getResourceAsStream(resource);

		// 创建会话工厂，传入mybatis的配置文件信息
		sqlSessionFactory = new SqlSessionFactoryBuilder()
				.build(inputStream);
	}

	@Test
	void testgetDepartment() {
		 //创建sqlSession
		SqlSession sqlSession = sqlSessionFactory.openSession();
		//创建对象，mybatis自动生成mapper代理对象
		DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
		//执行sql语句
		department Department = DepartmentMapper.getDepartmentNo("EM003");
		System.out.println(department);
	}

	@Test
	void testUpdateDepartment() {
	  //创建sqlSession
	  SqlSession sqlSession = sqlSessionFactory.openSession();
	  //创建对象，mybatis自动生成mapper代理对象
	  DepartmentMapper departmentMapper = sqlSession.getMapper(DepartmentMapper.class);
	  //执行sql语句
      department.setDepartmentName("市场部门");
      department.setDepartmentNo("EM002");
      sqlSession.commit();
      System.out.println(department);
		}

}
