/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.5.20 : Database - employee
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`employee` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `employee`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`username`,`password`) values ('a','a');

/*Table structure for table `t_department` */

DROP TABLE IF EXISTS `t_department`;

CREATE TABLE `t_department` (
  `departmentNo` varchar(20) NOT NULL COMMENT 'departmentNo',
  `departmentName` varchar(50) NOT NULL COMMENT '部门名称',
  PRIMARY KEY (`departmentNo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_department` */

insert  into `t_department`(`departmentNo`,`departmentName`) values ('BM001','财务部'),('BM002','市场部'),('BM003','开发部啊'),('BM005','人力部'),('BM006','安全部'),('BM007','产品部'),('BM009','设计部');

/*Table structure for table `t_employee` */

DROP TABLE IF EXISTS `t_employee`;

CREATE TABLE `t_employee` (
  `employeeNo` varchar(20) NOT NULL COMMENT 'employeeNo',
  `positionObj` int(11) NOT NULL COMMENT '职位',
  `name` varchar(20) NOT NULL COMMENT '姓名',
  `sex` varchar(4) NOT NULL COMMENT '性别',
  `employeePhoto` varchar(60) NOT NULL COMMENT '员工照片',
  `birthday` varchar(20) DEFAULT NULL COMMENT '出生日期',
  `schoolRecord` varchar(20) NOT NULL COMMENT '学历',
  `employeeDesc` varchar(5000) DEFAULT NULL COMMENT '员工介绍',
  PRIMARY KEY (`employeeNo`),
  KEY `positionObj` (`positionObj`),
  CONSTRAINT `t_employee_ibfk_1` FOREIGN KEY (`positionObj`) REFERENCES `t_position` (`positionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `t_employee` */

insert  into `t_employee`(`employeeNo`,`positionObj`,`name`,`sex`,`employeePhoto`,`birthday`,`schoolRecord`,`employeeDesc`) values ('EM003',5,'何嘉晟','男','upload/985684e1-2357-4be5-985b-c990e8818e5d.jpg','1998-01-01','本科','ConverseBoy'),('EM004',11,'魏邵杰','男','upload/NoImage.jpg','2019-06-11','小学',''),('EM005',13,'和昊','男','upload/NoImage.jpg','2019-06-12','小学',''),('EM006',7,'李翔','男','upload/NoImage.jpg','2019-06-05','本科',''),('EM007',1,'丁思远','男','upload/NoImage.jpg','2019-06-18','高中','');

/*Table structure for table `t_position` */

DROP TABLE IF EXISTS `t_position`;

CREATE TABLE `t_position` (
  `positionId` int(11) NOT NULL AUTO_INCREMENT COMMENT '职位id',
  `departmentObj` varchar(20) NOT NULL COMMENT '所属部门',
  `positionName` varchar(50) NOT NULL COMMENT '职位名称',
  `baseSalary` float NOT NULL COMMENT '基本工资',
  `sellPercent` varchar(20) NOT NULL COMMENT '销售提成',
  PRIMARY KEY (`positionId`),
  KEY `departmentObj` (`departmentObj`),
  CONSTRAINT `t_position_ibfk_1` FOREIGN KEY (`departmentObj`) REFERENCES `t_department` (`departmentNo`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `t_position` */

insert  into `t_position`(`positionId`,`departmentObj`,`positionName`,`baseSalary`,`sellPercent`) values (1,'BM001','财务会计',12000,'10%'),(2,'BM002','市场营销专员',10000,'18%'),(4,'BM003','技术总监',15000,'10%'),(5,'BM003','算法工程师',20000,'10%'),(6,'BM007','产品总监',20000,'10%'),(7,'BM002','销售员',12000,'15%'),(8,'BM003','前端工程师',15000,'10%'),(9,'BM006','保安',7000,'10%'),(10,'BM001','高级会计师',18000,'10%'),(11,'BM009','美工设计师',12000,'300'),(12,'BM009','UI设计师',15000,'600'),(13,'BM003','软件测试',14000,'10%');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
