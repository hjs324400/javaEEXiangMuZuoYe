# javaEE项目作业

#### 介绍
员工管理系统，采用技术: SpringMVC + Spring + MyBatis   前台技术: jquery + easyui框架

#### 软件架构
ssm框架

#### 使用说明
1 采用ajax方式异步提交数据，只用局部刷新，减少了带宽。
2 使用了hibernate提供的校验框架，对客户端数据进行校验！
3 Mybati数据库DAO层采用的是Mapper代理开发方法，输入映射采用的是POJO包装类型实现,输出映射采用了resultMap类型，实现了数据库多对一映射。
4 spring容器内部使用拦截器，以Spring AOP的方式实现事务控制管理。

#### 参与贡献
何嘉晟
魏邵杰
和昊


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)